function stripPrefix(str) {
    let prefixMatch = new RegExp(/(?!xmlns)^.*:/);
    return str.replace(prefixMatch, '');
}

module.exports = stripPrefix;