const NDOVInfoPlusParser = require('./NDOVInfoPlusParser');

class NDOVInfoPlusDVSParser extends NDOVInfoPlusParser {
    parseMessage(envelope, message, callback) {
        // Remove the container elements
        let parsed = message.PutReisInformatieBoodschapIn.ReisInformatieProductDAS;

        // Remove as much unwanted arrays as we can, but leave the more complicated ones in place for now
        parsed = this._remove1indexes(parsed, {
            blacklist: ['ReisTip', 'VerkorteRouteHerkomst', 'WijzigingHerkomst', 'PresentatieOpmerkingen'],
        });

        try {
            // Give the metadata a good place, instead of the weird "$" key
            parsed.Meta = parsed.$;
            delete parsed.$;

            parsed.DynamischeAankomstStaat.TreinAankomst.TreinSoort.Code = parsed.DynamischeAankomstStaat.TreinAankomst.TreinSoort.$.Code;
            delete parsed.DynamischeAankomstStaat.TreinAankomst.TreinSoort.$;

            parsed.DynamischeAankomstStaat.TreinAankomst.TreinHerkomst = this._parseStations(parsed.DynamischeAankomstStaat.TreinAankomst.TreinHerkomst);
            parsed.DynamischeAankomstStaat.TreinAankomst.AankomstTijd = this._parseTimes(parsed.DynamischeAankomstStaat.TreinAankomst.AankomstTijd);
            parsed.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor = this._parseTrack(parsed.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor);
            parsed.DynamischeAankomstStaat.TreinAankomst.VerkorteRouteHerkomst = this._parseRoute(parsed.DynamischeAankomstStaat.TreinAankomst.VerkorteRouteHerkomst);
            parsed.DynamischeAankomstStaat.TreinAankomst.WijzigingHerkomst = this._parseChanges(parsed.DynamischeAankomstStaat.TreinAankomst.WijzigingHerkomst);

            parsed.DynamischeAankomstStaat.TreinAankomst.TreinStatus = parseInt(parsed.DynamischeAankomstStaat.TreinAankomst.TreinStatus);
        }
        catch (e) {
            console.log(JSON.stringify(message, null, 2));
            console.error(e);
        }

        callback(parsed);
    }

    /**
     * Parses the changes.
     * These changes are indications of changes made to the planned train. They may contain messages for travelers.
     * @param {Array|null} changes - A list of all changes
     * @returns {*}
     * @protected
     */
    _parseChanges(changes) {
        if (this._isArray(changes)) {
            for (let i = 0; i < changes.length; i++) {
                let change = changes[i];

                change = this._remove1indexes(change, {
                    blacklist: ['PresentatieWijzigingHerkomst'],
                });

                if (change.PresentatieWijzigingHerkomst)
                    change.PresentatieWijzigingHerkomst = this._parseMessages(change.PresentatieWijzigingHerkomst, 'change', `CHG-${change.WijzigingType}`);

                changes[i] = change;
            }
        }

        return changes;
    }
}

module.exports = NDOVInfoPlusDVSParser;