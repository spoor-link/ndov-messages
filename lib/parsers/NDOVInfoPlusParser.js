const Parser = require('./Parser');
const moment = require('moment');

/**
 * NDOVInfoPlusParser is a base class for parsing messages coming from the NDOV dynamic updates stream.
 * It has a number of common methods to be used by the actual parsers.
 *
 * All element names are in Dutch, because the object given to the parser is Dutch, so it stays with the naming scheme.
 */
class NDOVInfoPlusParser extends Parser {
    /**
     * Parses travel tips from dynamic events.
     * These tips are part of the main "Trein" JSON element
     * @param {Array|null} travelTips - The array of travel tips or null-like if no travel tips are present
     * @returns {Array}
     * @protected
     */
    _parseTravelTips(travelTips) {
        let returnTips = [];

        if (this._isArray(travelTips)) {
            for (let i = 0; i < travelTips.length; i++) {
                // The "PresentatieReisTip" needs to be handled separately
                let travelTip = this._remove1indexes(travelTips[i], {
                    blacklist: ['PresentatieReisTip'],
                });

                let returnTravelTip = this._parseMessages(travelTip.PresentatieReisTip, 'tip', `TRAVTIP-${travelTip.ReisTipCode}`);

                // The travel tips have a subdivision of priority. The only priority given by the travel tip is the sub priority, so we add the correct fixed priority to it
                returnTravelTip.Prioriteit = 5 + (returnTravelTip.Prioriteit / 10);

                returnTips.push(returnTravelTip);
            }
        }

        return returnTips;
    }

    /**
     * Parses embarking tips from dynamic events.
     * These tips are part of the main "Trein" JSON element
     * @param {Array|null} embarkingTips - The array of embarking tips or null-like if no travel tips are present
     * @returns {Array}
     * @protected
     */
    _parseEmbarkingTips(embarkingTips) {
        let returnTips = [];

        if (this._isArray(embarkingTips)) {
            for (let i = 0; i < embarkingTips.length; i++) {
                let travelTip = this._remove1indexes(embarkingTips[i], {
                    blacklist: ['PresentatieInstapTip'],
                });

                returnTips.push(this._parseMessages(travelTip.PresentatieReisTip, 'tip', `EMBRKTIP-${travelTip.InstapTipCode}`));
            }
        }

        return returnTips;
    }

    /**
     * Parses transfer tips from dynamic events.
     * These tips are part of the main "Trein" JSON element
     * @param {Array|null} transferTips - The array of embarking tips or null-like if no travel tips are present
     * @returns {Array}
     * @protected
     */
    _parseTransferTips(transferTips) {
        let returnTips = [];

        if (this._isArray(transferTips)) {
            for (let i = 0; i < transferTips.length; i++) {
                let travelTip = this._remove1indexes(transferTips[i], {
                    blacklist: ['PresentatieOverstapTip'],
                });

                returnTips.push(this._parseMessages(travelTip.PresentatieOverstapTip, 'tip', `TRSTIP-${travelTip.OverstapTipCode}`));
            }
        }

        return returnTips;
    }

    /**
     * Parses elements that contain datetimes (both planned and current).
     * @param {Array} times - A list of the datetime objects
     * @returns {{Gepland, Actueel}}
     * @protected
     */
    _parseTimes(times) {
        let plannedTime = {};
        let currentTime = {};

        if (this._isArray(times)) {
            for (let i = 0; i < times.length; i++) {
                let departureTime = this._remove1indexes(times[i]);

                if (departureTime.$.InfoStatus === 'Gepland') {
                    plannedTime = moment(departureTime._);
                }
                else if (departureTime.$.InfoStatus === 'Actueel') {
                    currentTime = moment(departureTime._);
                }
            }
        }

        times = {
            Gepland: plannedTime,
            Actueel: currentTime,
        };

        return times;
    }

    /**
     * Parses the origin or destination of dynamic events.
     * The destination gets split between the planned and current destination, which are not easy to get from the raw JSON
     * @param {Array|null} stations - The list of stations (usually 2, planned and current)
     * @returns {{Gepland, Actueel}}
     * @protected
     */
    _parseStations(stations) {
        let plannedStation = {};
        let currentStation = {};

        if (this._isArray(stations)) {
            for (let i = 0; i < stations.length; i++) {
                let station = this._remove1indexes(stations[i]);

                if (station.$.InfoStatus === 'Gepland') {
                    delete station.$;
                    plannedStation = station;
                }
                else if (station.$.InfoStatus === 'Actueel') {
                    delete station.$;
                    currentStation = station;
                }
            }
        }

        stations = {
            Gepland: plannedStation,
            Actueel: currentStation,
        };

        return stations;
    }

    /**
     * Parses the tracks of dynamic events.
     * The track gets split between the planned and current track, which are not easy to get from the raw JSON
     * @param {Array|null} tracks - The list of tracks (usually 2, planned and current)
     * @returns {{Gepland, Actueel}}
     * @protected
     */
    _parseTrack(tracks) {
        let plannedTrack = {};
        let currentTrack = {};

        if (this._isArray(tracks)) {
            for (let i = 0; i < tracks.length; i++) {
                let track = this._remove1indexes(tracks[i]);

                if (track.$.InfoStatus === 'Gepland') {
                    delete track.$;

                    if(!plannedTrack.SpoorNummer)
                        plannedTrack = track;
                    else {
                        plannedTrack.SpoorNummer += '-'+track.SpoorNummer;
                    }
                }
                else if (track.$.InfoStatus === 'Actueel') {
                    delete track.$;

                    if(!currentTrack.SpoorNummer)
                        currentTrack = track;
                    else {
                        currentTrack.SpoorNummer += '-'+track.SpoorNummer;
                    }
                }
            }
        }

        tracks = {
            Gepland: plannedTrack,
            Actueel: currentTrack,
        };

        return tracks;
    }

    /**
     * This parses the route summary given by the event
     * @param {Array|null} routes - The list of routes (usually 2, planned and current)
     * @returns {{Gepland, Actueel}}
     * @protected
     */
    _parseRoute(routes) {
        let plannedRoute = {};
        let currentRoute = {};

        if (this._isArray(routes)) {
            for (let i = 0; i < routes.length; i++) {
                let route = this._remove1indexes(routes[i]);

                if (!this._isArray(route.Station)) // Small hack to make sure everything gets cleaned, but the stations list will always be a list
                    route.Station = [route.Station];

                if (route.$.InfoStatus === 'Gepland') {
                    plannedRoute = route.Station;
                }
                else if (route.$.InfoStatus === 'Actueel') {
                    currentRoute = route.Station;
                }
            }
        }

        routes = {
            Gepland: plannedRoute,
            Actueel: currentRoute,
        };

        return routes;
    }

    /**
     * Used to parse all messages from elements which contain them
     * @param {object} messages - The messages object. This should contain the "Uitingen" attribute
     * @param {string} type - The type of messages it contains (change or tip)
     * @param {string} code - The message code (for example: CHG-10)
     * @returns {{Type: string, Code: string, Prioriteit: number, Uitingen: Array}}
     * @protected
     */
    _parseMessages(messages, type, code) {
        messages = this._removeArray(messages);

        let returnMessages = {
            Type: type,
            Code: code,
            Prioriteit: 0,
            Uitingen: [],
        };

        if (this._isArray(messages.Uitingen)) {
            for (let i = 0; i < messages.Uitingen.length; i++) {
                let message = this._remove1indexes(messages.Uitingen[i]);
                let language = '';

                if(typeof message.$ !== 'undefined') {
                    if (message.$.Taal === 'nl')
                        language = 'nl_NL';
                    else if (message.$.Taal === 'en')
                        language = 'en_GB';
                }

                let uiting = message.Uiting;
                let messageText = '';

                if(typeof uiting !== 'undefined') {
                    returnMessages.Prioriteit = parseInt(uiting.$.Prioriteit);
                    messageText = uiting._;
                }

                returnMessages.Uitingen.push({
                    Taal: language,
                    Uiting: messageText,
                });
            }
        }

        return returnMessages;
    }
}

module.exports = NDOVInfoPlusParser;