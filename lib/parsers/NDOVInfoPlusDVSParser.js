const NDOVInfoPlusParser = require('./NDOVInfoPlusParser');

class NDOVInfoPlusDVSParser extends NDOVInfoPlusParser {
    parseMessage(envelope, message, callback) {
        // Remove the container elements
        let parsed = message.PutReisInformatieBoodschapIn.ReisInformatieProductDVS;

        // Remove as much unwanted arrays as we can, but leave the more complicated ones in place for now
        parsed = this._remove1indexes(parsed, {
            blacklist: ['ReisTip', 'VerkorteRoute', 'TreinVleugel', 'MaterieelDeelDVS', 'Wijziging', 'PresentatieOpmerkingen'],
        });

        try {
            // Give the metadata a good place, instead of the weird "$" key
            parsed.Meta = parsed.$;
            delete parsed.$;

            parsed.DynamischeVertrekStaat.Trein.TreinSoort.Code = parsed.DynamischeVertrekStaat.Trein.TreinSoort.$.Code;
            delete parsed.DynamischeVertrekStaat.Trein.TreinSoort.$;

            parsed.DynamischeVertrekStaat.Trein.InstapTip = this._parseEmbarkingTips(parsed.DynamischeVertrekStaat.Trein.InstapTip);
            parsed.DynamischeVertrekStaat.Trein.OverstapTip = this._parseTransferTips(parsed.DynamischeVertrekStaat.Trein.OverstapTip);
            parsed.DynamischeVertrekStaat.Trein.ReisTip = this._parseTravelTips(parsed.DynamischeVertrekStaat.Trein.ReisTip);
            parsed.DynamischeVertrekStaat.Trein.TreinEindBestemming = this._parseStations(parsed.DynamischeVertrekStaat.Trein.TreinEindBestemming);
            parsed.DynamischeVertrekStaat.Trein.VertrekTijd = this._parseTimes(parsed.DynamischeVertrekStaat.Trein.VertrekTijd);
            parsed.DynamischeVertrekStaat.Trein.TreinVertrekSpoor = this._parseTrack(parsed.DynamischeVertrekStaat.Trein.TreinVertrekSpoor);
            parsed.DynamischeVertrekStaat.Trein.VerkorteRoute = this._parseRoute(parsed.DynamischeVertrekStaat.Trein.VerkorteRoute);
            parsed.DynamischeVertrekStaat.Trein.TreinVleugel = this._parseTrainWings(parsed.DynamischeVertrekStaat.Trein.TreinVleugel);
            parsed.DynamischeVertrekStaat.Trein.Wijziging = this._parseChanges(parsed.DynamischeVertrekStaat.Trein.Wijziging);

            parsed.DynamischeVertrekStaat.Trein.TreinStatus = parseInt(parsed.DynamischeVertrekStaat.Trein.TreinStatus);
        }
        catch (e) {
            console.log(JSON.stringify(message, null, 2));
            console.error(e);
        }

        callback(parsed);
    }

    _parseTrainWings(wings) {
        if (this._isArray(wings)) {
            for (let i = 0; i < wings.length; i++) {
                wings[i] = this._remove1indexes(wings[i], {
                    blacklist: ['StopStations', 'MaterieelDeelDVS'],
                });

                wings[i].TreinVleugelVertrekSpoor = this._parseTrack(wings[i].TreinVleugelVertrekSpoor);
                wings[i].TreinVleugelEindBestemming = this._parseStations(wings[i].TreinVleugelEindBestemming);
                wings[i].StopStations = this._parseRoute(wings[i].StopStations);
                wings[i].MaterieelDeelDVS = this._parseWingRollingStock(wings[i].MaterieelDeelDVS);
            }
        }

        return wings;
    }

    _parseWingRollingStock(rollingStock) {
        if (this._isArray(rollingStock)) {
            for (let i = 0; i < rollingStock.length; i++) {
                rollingStock[i] = this._remove1indexes(rollingStock[i]);

                rollingStock[i].MaterieelDeelEindBestemming = this._parseStations(rollingStock[i].MaterieelDeelEindBestemming);
            }
        }

        return rollingStock;
    }

    /**
     * Parses the changes.
     * These changes are indications of changes made to the planned train. They may contain messages for travelers.
     * @param {Array|null} changes - A list of all changes
     * @returns {*}
     * @protected
     */
    _parseChanges(changes) {
        if (this._isArray(changes)) {
            for (let i = 0; i < changes.length; i++) {
                let change = changes[i];

                change = this._remove1indexes(change, {
                    blacklist: ['PresentatieWijziging'],
                });

                if (change.PresentatieWijziging)
                    change.PresentatieWijziging = this._parseMessages(change.PresentatieWijziging, 'change', `CHG-${change.WijzigingType}`);

                changes[i] = change;
            }
        }

        return changes;
    }
}

module.exports = NDOVInfoPlusDVSParser;