const Parser = require('./Parser');
const moment = require('moment');

class NDOVGPSParser extends Parser {
    parseMessage(envelope, message, callback) {
        let parsed = message.ArrayOfTreinLocation;

        try {
            // Give the metadata a good place, instead of the weird "$" key
            parsed.Meta = parsed.$;
            delete parsed.$;

            for (let i = 0; i < parsed.TreinLocation.length; i++) {
                let train = parsed.TreinLocation[i];
                train.TreinNummer = this._removeArray(train.TreinNummer);

                for (let j = 0; j < train.TreinMaterieelDelen.length; j++) {
                    let stock = this._remove1indexes(train.TreinMaterieelDelen[j]);
                    stock.Materieelvolgnummer = parseInt(stock.Materieelvolgnummer);
                    stock.GpsDatumTijd = moment(stock.GpsDatumTijd);
                    stock.Orientatie = parseInt(stock.Orientatie);
                    stock.Fix = parseInt(stock.Fix);
                    stock.Longitude = parseFloat(stock.Longitude);
                    stock.Latitude = parseFloat(stock.Latitude);
                    stock.Elevation = parseFloat(stock.Elevation);
                    stock.Snelheid = parseFloat(stock.Snelheid);
                    stock.Richting = parseFloat(stock.Richting);
                    stock.Hdop = parseFloat(stock.Hdop);
                    stock.AantalSatelieten = parseInt(stock.AantalSatelieten);

                    train.TreinMaterieelDelen[j] = stock;
                }

                parsed.TreinLocation[i] = train;
            }
        }
        catch (e) {
            console.log(JSON.stringify(message, null, 2));
            console.error(e);
        }

        callback(parsed);
    }
}

module.exports = NDOVGPSParser;