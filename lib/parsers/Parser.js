class Parser {
    constructor() {

    }

    /**
     * Removes all arrays with only a single value inside it recursively.
     * It replaces the array with the first and only value.
     * It leaves arrays with multiple values as is (but does check within them)
     * @param input your input array
     * @param opts a JSON object for options
     * @returns {*}
     * @protected
     */
    _remove1indexes(input, opts) {
        // Check opts settings
        if (typeof opts !== 'object')
            opts = {};
        if (typeof opts.blacklist === 'undefined')
            opts.blacklist = [];

        if (this._isArray(input)) {
            if (input.length === 1) {
                return this._remove1indexes(this._removeArray(input), opts);
            }
            else {
                for (let i = 0; i < input.length; i++) {
                    input[i] = this._remove1indexes(input[i], opts);
                }

                return input;
            }
        }
        else if (typeof input === 'object') {
            for (let key in input) {
                if (!opts.blacklist.includes(key))
                    input[key] = this._remove1indexes(input[key], opts);
            }

            return input;
        }
        else {
            return input;
        }
    }

    /**
     * Removes the array by replacing the input with the first array value
     * @param input
     * @returns {*}
     * @protected
     */
    _removeArray(input) {
        if (input[0])
            return input[0];
        else
            return null;
    }

    /**
     * Checks if the input is an array
     * @param input
     * @returns {boolean}
     * @protected
     */
    _isArray(input) {
        return (!!input) && (input.constructor === Array);
    };
}

module.exports = Parser;