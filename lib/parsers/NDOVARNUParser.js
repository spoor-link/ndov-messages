const Parser = require('./Parser');

class NDOVARNUParser extends Parser {
    parseMessage(envelope, message, callback) {
        // Remove the container elements
        let parsed = message.PutServiceInfoIn.ServiceInfoList;
        parsed = this._removeArray(parsed);

        try {
            for (let i = 0; i < parsed.ServiceInfo.length; i++) {
                this._parseServiceInfo(parsed.ServiceInfo[i]);
            }
        }
        catch (e) {
            console.log(JSON.stringify(message, null, 2));
            console.error(e);
        }

        callback(parsed);
    }

    _parseServiceInfo(serviceInfo) {
        serviceInfo = this._remove1indexes(serviceInfo, {
            blacklist: ['Stop'],
        });
        serviceInfo.StopList = this._parseStopList(serviceInfo.StopList);

        return serviceInfo;
    }

    _parseStopList(stopList) {
        let stops = [];

        if (this._isArray(stopList.Stop)) {
            for (let i = 0; i < stopList.Stop.length; i++) {
                stops.push(this._remove1indexes(stopList.Stop[i]));
            }
        }

        return stops;
    }
}

module.exports = NDOVARNUParser;