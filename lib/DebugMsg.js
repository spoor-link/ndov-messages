const moment = require('moment');

String.prototype.ljust = function (width, padding) {
    padding = padding || " ";
    padding = padding.substr(0, 1);
    if (this.length < width)
        return this + padding.repeat(width - this.length);
    else
        return this.toString();
};

class DebugMsg {
    static displayMessage(envelope, result) {
        let now = moment();

        if (envelope === '/RIG/InfoPlusDVSInterface4') {
            let DVS = result.PutReisInformatieBoodschapIn.ReisInformatieProductDVS[0].DynamischeVertrekStaat[0];
            let stationTime = moment(DVS.Trein[0].VertrekTijd[0]._);
            console.log(
                '| ' + DVS.RitDatum[0].ljust(10) +
                ' | ' + now.format('HH:mm:ss') +
                ' | ' + envelope.ljust(30) +
                ' | ' + DVS.Trein[0].TreinNummer[0].ljust(7) +
                ' | ' + DVS.RitStation[0].StationCode[0].toLowerCase().ljust(5) +
                ' | ' + stationTime.format('YYYY-MM-DD HH:mm:ss') +
                ' | ' + moment.duration(DVS.Trein[0].ExacteVertrekVertraging[0]).asSeconds().toString().ljust(4) +
                ' | ' + DVS.Trein[0].TreinStatus[0].ljust(1) +
                ' |'
            );
        }
        else if (envelope === '/RIG/InfoPlusDASInterface4') {
            let DAS = result.PutReisInformatieBoodschapIn.ReisInformatieProductDAS[0].DynamischeAankomstStaat[0];
            let stationTime = moment(DAS.TreinAankomst[0].AankomstTijd[0]._);
            console.log(
                '| ' + DAS.RitDatum[0].ljust(10) +
                ' | ' + now.format('HH:mm:ss') +
                ' | ' + envelope.ljust(30) +
                ' | ' + DAS.RitId[0].ljust(7) +
                ' | ' + DAS.RitStation[0].StationCode[0].toLowerCase().ljust(5) +
                ' | ' + stationTime.format('YYYY-MM-DD HH:mm:ss') +
                ' | ' + moment.duration(DAS.TreinAankomst[0].ExacteAankomstVertraging[0]).asSeconds().toString().ljust(4) +
                ' | ' + DAS.TreinAankomst[0].TreinStatus[0].ljust(1) +
                ' |'
            );
        }
        else if (envelope === '/RIG/InfoPlusPILInterface5') {
            let PIL = result.PutReisInformatieBoodschapIn.ReisInformatieProductPatroonInformatieLandelijk[0].LandelijkePatroonInformatie[0];
            let stationTime = moment(PIL.VrijgaveTijd[0]);
            console.log(
                '| ' + ''.ljust(10) +
                ' | ' + now.format('HH:mm:ss') +
                ' | ' + envelope.ljust(30) +
                ' | ' + PIL.DossierNummer[0].ljust(7) +
                ' | ' + 'NLD'.ljust(5) +
                ' | ' + stationTime.format('YYYY-MM-DD HH:mm:ss') +
                ' | ' + PIL.VerstoringFase[0].ljust(4) +
                ' | ' + PIL.VerstoringImpact[0].ljust(1) +
                ' |'
            );
        }
        else if (envelope === '/RIG/InfoPlusPISInterface5') {
            let PIS = result.PutReisInformatieBoodschapIn.ReisInformatieProductPatroonInformatieStation[0].StationPatroonInformatie[0];
            let stationTime = moment(PIS.VrijgaveTijd[0]);
            console.log(
                '| ' + ''.ljust(10) +
                ' | ' + now.format('HH:mm:ss') +
                ' | ' + envelope.ljust(30) +
                ' | ' + PIS.DossierNummer[0].ljust(7) +
                ' | ' + PIS.Station[0].StationCode[0].toLowerCase().ljust(5) +
                ' | ' + stationTime.format('YYYY-MM-DD HH:mm:ss') +
                ' | ' + PIS.VerstoringFase[0].ljust(4) +
                ' | ' + PIS.VerstoringImpact[0].ljust(1) +
                ' |'
            );
        }
        else if (envelope === '/RIG/NStreinpositiesInterface5') {
            console.log(
                '| ' + ''.ljust(10) +
                ' | ' + now.format('HH:mm:ss') +
                ' | ' + envelope.ljust(30) +
                ' | ' + ''.ljust(7) +
                ' | ' + ''.ljust(5) +
                ' | ' + ''.ljust(19) +
                ' | ' + ''.ljust(4) +
                ' | ' + ''.ljust(1) +
                ' |'
            );
        }
        else if (envelope === '/RIG/ARNURitinfo') {
            let ARNU = result.PutServiceInfoIn.ServiceInfoList[0].ServiceInfo;

            for (let i = 0; i < ARNU.length; i++) {
                let message = ARNU[i];

                let secondColumn = '';
                if (i === 0)
                    secondColumn = now.format('HH:mm:ss');
                else if (i < ARNU.length - 1)
                    secondColumn = '    ├────';
                else
                    secondColumn = '    └────';

                console.log(
                    '| ' + ''.ljust(10) +
                    ' | ' + secondColumn.ljust(8) +
                    ' | ' + envelope.ljust(30) +
                    ' | ' + message.ServiceCode[0].ljust(7) +
                    ' | ' + ''.ljust(5) +
                    ' | ' + ''.ljust(19) +
                    ' | ' + ''.ljust(4) +
                    ' | ' + ''.ljust(1) +
                    ' |'
                );
            }
        }
        else {
            console.log(
                '| ' + ''.ljust(10) +
                ' | ' + now.format('HH:mm:ss') +
                ' | ' + envelope.ljust(30) +
                ' | ' + ''.ljust(7) +
                ' | ' + ''.ljust(5) +
                ' | ' + ''.ljust(19) +
                ' | ' + ''.ljust(4) +
                ' | ' + ''.ljust(1) +
                ' |'
            );
        }
    }
}

module.exports = DebugMsg;