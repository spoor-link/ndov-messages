const EventEmitter = require('events');
const zmq = require('zeromq');
const zlib = require('zlib');
const xml2js = require('xml2js');
const stripPrefix = require('./stripPrefix');
const DebugMsg = require('./DebugMsg');

const NDOVInfoPlusDVSParser = require('./parsers/NDOVInfoPlusDVSParser');
const NDOVInfoPlusDASParser = require('./parsers/NDOVInfoPlusDASParser');
const NDOVARNUParser = require('./parsers/NDOVARNUParser');
const NDOVGPSParser = require('./parsers/NDOVGPSParser');

class NDOVMessages extends EventEmitter {
    constructor(opts) {
        super();

        this.listeners = {};
        this.options = {};

        this._parseOptions(opts);
    }

    addListener(name, address, envelope) {
        let listener = zmq.socket('sub');
        listener.connect(address);
        listener.subscribe(envelope);

        listener.on('message', (envelope, message) => {
            this._parseMessage(envelope, message);
        });

        this.listeners[name] = listener;

        this.emit('listener.added');
    }

    _parseOptions(opts) {
        if(typeof opts !== 'object')
            opts = {};

        opts.debug = !!opts.debug;

        this.options = opts;
    }

    _parseMessage(envelope, message) {
        envelope = envelope.toString();

        zlib.unzip(message, (err, buffer) => {
            if (err)
                console.error(err);

            if (buffer) {
                message = buffer.toString();

                xml2js.parseString(buffer.toString(), {
                    tagNameProcessors: [stripPrefix],
                }, (err, result) => {
                    if (err)
                        console.error(err);

                    if (result) {
                        // Show debug message
                        if(this.options.debug)
                            DebugMsg.displayMessage(envelope, result);

                        this._parseJSONMessage(envelope, result);
                    }
                });
            }
        });
    }

    _parseJSONMessage(envelope, message) {
        let parsers = this._getParsers();

        // Check if a parser is available to clean up the message
        if (typeof parsers[envelope] !== 'undefined') {
            let parser = new parsers[envelope]();
            parser.parseMessage(envelope, message, parsed => {
                this.emit('message', envelope, parsed);
            })
        }
        else {
            this.emit('message', envelope, message);
        }
    }

    _getParsers() {
        return {
            '/RIG/InfoPlusDASInterface4': NDOVInfoPlusDASParser,
            '/RIG/InfoPlusDVSInterface4': NDOVInfoPlusDVSParser,
            '/RIG/ARNURitinfo': NDOVARNUParser,
            '/RIG/NStreinpositiesInterface5': NDOVGPSParser,
        };
    }
}

module.exports = NDOVMessages;